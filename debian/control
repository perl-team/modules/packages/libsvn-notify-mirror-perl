Source: libsvn-notify-mirror-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Niko Tyni <ntyni@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: perl,
                     libsvn-notify-perl,
                     libyaml-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsvn-notify-mirror-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsvn-notify-mirror-perl.git
Homepage: https://metacpan.org/release/SVN-Notify-Mirror

Package: libsvn-notify-mirror-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libsvn-notify-perl,
         libyaml-perl
Suggests: libfile-rsync-perl,
          libnet-ssh-perl
Description: module to keep a mirrored working copy of a repository path
 SVN::Notify::Mirror allows one to keep a directory in sync with a portion of
 a Subversion repository. It is typically used to keep a development web
 server in sync with the changes made to the repository. This directory can
 either be on the same box as the repository itself, or it can be remote (via
 SSH connection).
